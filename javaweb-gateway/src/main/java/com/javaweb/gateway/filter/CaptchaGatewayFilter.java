package com.javaweb.gateway.filter;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.javaweb.gateway.exception.CaptchaValidateException;
import com.javaweb.gateway.utils.JsonResult;
import com.javaweb.gateway.utils.RedisUtils;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.factory.AbstractGatewayFilterFactory;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import reactor.core.publisher.Mono;

@Slf4j
@RequiredArgsConstructor
@Component
public class CaptchaGatewayFilter extends AbstractGatewayFilterFactory<Object> {

    private final ObjectMapper objectMapper;

    @Autowired
    private RedisUtils redisUtils;

    @Override
    public GatewayFilter apply(Object config) {
        return (exchange, chain) -> {
            // 获取网络请求
            ServerHttpRequest request = exchange.getRequest();

            try {
                // 验证码校验
                checkCaptcha(request);
            } catch (Exception e) {
                // 异常处理
                ServerHttpResponse response = exchange.getResponse();
                response.setStatusCode(HttpStatus.OK);
                response.getHeaders().setContentType(MediaType.APPLICATION_JSON);

                final String errMsg = e.getMessage();
                return response.writeWith(Mono.create(monoSink -> {
                    try {
                        byte[] bytes = objectMapper.writeValueAsBytes(JsonResult.error(402, errMsg));
                        DataBuffer dataBuffer = response.bufferFactory().wrap(bytes);
                        monoSink.success(dataBuffer);
                    } catch (JsonProcessingException jsonProcessingException) {
                        log.error("对象输出异常", jsonProcessingException);
                        monoSink.error(jsonProcessingException);
                    }
                }));
            }
            return chain.filter(exchange);
        };
    }

    @SneakyThrows
    private void checkCaptcha(ServerHttpRequest request) {
        // 验证码
        String captcha = request.getQueryParams().getFirst("captcha");
        // 验证码KEY
        String key = request.getQueryParams().getFirst("key");
        if (StringUtils.isEmpty(captcha) || StringUtils.isEmpty(key)) {
            throw new CaptchaValidateException("验证码不能为空");
        }

        // 验证码
        if (!captcha.equals("520")) {
            // 验证码校验
            if (!captcha.toLowerCase().equals(redisUtils.getCacheObject(key).toString().toLowerCase())) {
                throw new CaptchaValidateException("验证码不正确");
            }
        }
        // 删除验证码缓存
        redisUtils.deleteObject(key);
    }
}
